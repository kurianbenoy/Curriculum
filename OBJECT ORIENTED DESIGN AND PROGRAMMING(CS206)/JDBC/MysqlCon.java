import java.sql.*;
import java.io.*;

class MysqlCon {

    public static void main(String args[]) {
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/FIRST","root","pwd");

            String sql = "select * from employee where empno=23 and name =
                "kurian" ";

            PreparedStatement preparedStatement = 
                con.prepareStatement(sql);

            BufferedReader read = new BufferedReader(new
                    InputStreamReader(System.in));
            System.out.println("Enter Eno");
            String empno = read.readLine();
            System.out.println("Enter the name");
            String name = read.readLine();

            preparedStatement.setString(1,empno);
            preparedStatement.setString(2,name);

            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next())
                System.out.println(rs.getlnt(1) + " " + rs.getlnt(2) + " " +
                        rs.getlnt(3));

                con.close();
        }catch(Exception e) {
            System.out.println(e);
        }
    }
