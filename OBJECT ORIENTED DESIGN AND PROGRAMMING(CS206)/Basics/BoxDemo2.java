class Box {
    double w;
    double h;
    double d;
}

class BoxDemo2 {

    public static void main(String args[]) {
        Box b1 = new Box();
        Box b2 = b1;
        double vol;

        b1.w =10;b1.d=100;b1.h=21;
        vol =(b1.w*b1.d*b1.h);
        System.out.println("Volume of box1 :" + vol);


        b2.w =6;
        b2.d=2;
        b2.h=9;
        vol =(b2.w*b2.d*b2.h); 
        System.out.println("Volume of box2 :" + vol);
    }
}
