class Box {
    double w;
    double h;
    double d;

    Box() {
        w =10;
        h=10;
        d=10;
    }

    double volume(){
        return w*d*h;
    }
}

class BoxDemo {

    public static void main(String args[]) {
        Box b1 = new Box();
        Box b2 = new Box();
        double vol;

        b1.w =10;b1.d=100;b1.h=21;
        vol =(b1.w*b1.d*b1.h);
        System.out.println("Volume of box1 :" + vol);


        vol = b2.volume();
        System.out.println("Volume of box2 :" + vol);
    }
}

