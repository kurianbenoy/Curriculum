class ForEach {
    public static void main(String args[]) {
        int num[] = { 2,5,7,2,1,9,4,7,13};
        int sum = 0;
        for(int x: num) {
            sum +=x;
        }

        System.out.println("Summation " + sum);
    }
}
