/* Author : Kurian Benoy 
 An applet program to display lines , rectangles and Oval shape
 */

import java.awt.*;
import java.applet.*;

/* <applet code="ColorDemo" width=300 height=500>
   </applet>
   */


public class ColorDemo extends Applet {

    public void paint(Graphics g) {

        Color c1 = new Color(350,23,100);
        Color c2 = new Color(100,250,90);
        Color c3 = new Color(100,50,23);

        g.setColor(c1);g.drawLine(0,0,100,100);
        g.drawLine(4,100,23,0);

        g.setColor(c2);
        g.drawLine(40,23,23,1);
        g.drawLine(4,23,52,1);

        g.setColor(Color.red);
        g.drawOval(10,349,2323,1);
        g.fillOval(70,90,140,23);

        g.setColor(Color.blue);
        g.drawOval(23,5,10,2);
        g.drawRect(10,32,5,10);
    }
}
