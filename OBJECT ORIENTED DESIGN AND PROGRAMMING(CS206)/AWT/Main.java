import javax.swing.*;

class swingdemo {
    swingdemo() {
        JFrame jf = new JFrame("Swing appl");
        jf.setSize(200,43);
        JLabel jl = new JLabel("Name",JLabel.CENTER);
        jf.add(jl);
        jf.setVisible(true);
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}

public class Main {
    public static void main(String args[]){
        new swingdemo();
    }
}
