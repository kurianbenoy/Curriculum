import java.io.*;

class CopyFile {
    public static void main(String args[]) throws IOException  {

        int i;
        FileInputStream fin = null;
        FileOutputStream fout = null ;

        if(args.length ! = 2) {
            System.out.println("Usage : Copy file from to ");
            return ;
        }

        try {

            fin = new FileInputStream(args[0]);
            fout = new FileOutputStream(args[1]);

            do {
                i = fin.read();
                if(i != -1) fout.write();
            } while(i!=-1);
        }catch(IOException e);

        System.out.println("IO error" + e);
    }
    finally {
        try {
            if(fin != null) fin.close();
            catch (IOException e2) {
                System.out.println("Error closinbg output file");
            }
        }
    }
}
