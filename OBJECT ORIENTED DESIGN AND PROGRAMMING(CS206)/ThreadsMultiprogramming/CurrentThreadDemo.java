class CurrentThreadDemo {
    public static void main(String args[]) {

        Thread t = Thread.currentThread();
        System.out.println("Current thread: " + t);
//        t.setName("KBThread");
        System.out.println("After name change : " + t);

        try {
            for(int n = 5;n>0;n--) {
                System.out.println(n);
                t.sleep(1000);
            }
        }catch(InterruptedException e) {
            System.out.print("Main thread is interrupted");
        }
    }
}

