class NewThread extends Thread {
    NewThread() {
        super("Demo Thread");
        System.out.println("Child Thread" );
        start();
    }

    public void run() {
        try {
            for(int i=5;i>0;i--) {
                System.out.println("Child thread: " + i);
                Thread.sleep(500);
            }
        }catch(InterruptedException e) {
            System.out.println("Child interrupted");
        }
        System.out.println("Exiting child thread. ");
    }
}

class ExtendThread {
    public static void main(String args[]) {
        new NewThread();

        try { 
            for(int i=5;i>0;i--) {
                System.out.println("Main Thread:" + i);
                Thread.sleep(100);
            }
        }catch(InterruptedException e) {
            System.out.println("Interrupted Exception");
        }
        System.out.print("Main thread going away ");
    }
}
